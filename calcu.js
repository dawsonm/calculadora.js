const btnadd = document.querySelector(".btnadd")

var notas = []

var ul = document.querySelector(".notas")
var contador = 0

var calcula = document.querySelector(".calcmedia")
let soma = 0
calcula.addEventListener("click", (event) => {
    event.preventDefault()
    for(let i =0; i < notas.length; i++){
        soma+=parseFloat(notas[i]);
    }

    let media = soma/(notas.length);
    const resultado = document.querySelector(".resultado")
    resultado.textContent = media.toFixed(2);

})



btnadd.addEventListener("click", (event) => {
    event.preventDefault()
    var input = document.querySelector(".num")
    if(input.value == ""){
        alert("Por favor, insira uma nota!")
    }else{
        notas.push(input.value)
        contador+=1
        ul.innerHTML +=`<li>A nota ${contador} foi: ${input.value} </li>`
        input.value = ''
        input.focus();
    }
   
})

